<?php
//This file connects the user to the database.
//It is referenced at the top of other pages as well to connect them to the database.

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "devotionList";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $dbname);