<?php

require_once 'db_connector.php';
require_once 'showTopMenu.php';

$searchForTopic = $_GET['devotionTopic'];
$searchForTitle = $_GET['devotionTitle'];
$searchForBody = $_GET['devotionBody'];

$sql_statement = "SELECT devotion_table.*, users_table.id as user_id, users_table.user_name  FROM `devotion_table` JOIN `users_table` ON users_table.id = devotion_table.users_table_id WHERE `devotion_topic` LIKE '%$searchForTopic%' 
AND `devotion_title` LIKE '%$searchForTitle%' AND `devotion_body` LIKE '%$searchForBody%'";

// The section below will fetch data from the table so that users can find devotions they created

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
                  
?>
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>

<form action = "processNewComment.php">
<input type = "hidden" name = "id" value = "<?php echo $row['id'];?>"></input>
<div class="row">
	<div class="leftcolumn">
		<div class="card">
			<h1><?php echo $row['devotion_topic'] . " [id #" . $row['id'] . "]";?></h1>
			<h5><?php echo "-" . $row['devotion_title'] . "- [Devotion by " . $row['user_name'] . "]";?></h5>
			<div class="fakeimg" style="height:200px;"><?php echo $row['devotion_body'];?></div>
		</div>
	</div>
</div><hr>

<div class="rightcolumn">
	<div class="card">
		<h2>Add Comment</h2>
		<textarea class= "fakeimg" name= "comments" placeholder="comments" rows="5" cols= "50"></textarea>
		<button type= "submit">Submit Comments</button>
	</div>
</div>

</form>  
  
<?php 

$devotion_id = $row['id'];
$sql_statement_comments = "SELECT * FROM `comments_table` JOIN `users_table` ON users_table.id = comments_table.users_table_id WHERE `devotion_table_id` = '$devotion_id'";
$result_comments = mysqli_query($connection, $sql_statement_comments);
if ($result_comments) {
    while( $row_comments = mysqli_fetch_array($result_comments)) {
        echo $row_comments['comment_text'] . "<br>";
        echo "comment made by user " . $row_comments['user_name'] . "<br>";
    }
}

        }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
    } else {
        echo "Error connecting " . mysqli_connect_error();
    }

?>